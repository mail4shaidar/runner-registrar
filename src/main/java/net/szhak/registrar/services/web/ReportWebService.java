package net.szhak.registrar.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.services.reports.ReportService;
import net.szhak.registrar.utils.dto.report.RunsForRunnerOutDTO;
import net.szhak.registrar.utils.dto.report.StatsForRunnerOutDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReportWebService {

    private final ReportService service;

    public RunsForRunnerOutDTO runsForRunner(Long id, LocalDate from, LocalDate to) {
        return service.runsForRunner(id, from, to);
    }

    public StatsForRunnerOutDTO statsForRunner(Long id, LocalDate from, LocalDate to) {
        return service.statsForRunner(id, from, to);
    }
}
