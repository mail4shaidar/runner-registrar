package net.szhak.registrar.services.web;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.AbstractEntity;
import net.szhak.registrar.exceptions.RequestParamsNotProvidedException;
import net.szhak.registrar.exceptions.RequestParamsOutOfScopeException;
import net.szhak.registrar.services.generic.AbstractService;
import net.szhak.registrar.utils.dto.in.AbstractInDTO;
import net.szhak.registrar.utils.dto.out.AbstractOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Slf4j
public abstract class AbstractWebService<E extends AbstractEntity,
        I extends AbstractInDTO,
        O extends AbstractOutDTO,
        R extends JpaRepository<E, Long>,
        S extends AbstractService<E, I, O, R>> {

    protected S service;

    @Autowired
    private void setService(S service) {
        this.service = service;
    }

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    public List<O> getAll(Integer page, Integer size) {
        if (page != null && size != null) {
            if (page >= 0 && size > 0) {
                return service.getAll(page, size).stream()
                        .map(service::entityToOutDTO)
                        .toList();
            } else {
                throw new RequestParamsOutOfScopeException("Size should be above zero and page should not be less then zero.");
            }
        } else if (page != null || size != null) {
            throw new RequestParamsNotProvidedException("Size and page should be both specified or null.");
        } else {
            return service.getAll().stream()
                    .map(service::entityToOutDTO)
                    .toList();
        }
    }

    public O getById(Long id) {
        return service.entityToOutDTO(service.getById(id));
    }

    public void deleteById(Long id) {
        service.deleteById(id);
    }

    public O create(I dto) {
        return service.entityToOutDTO(service.create(dto));
    }

    public O update(I dto) {
        return service.entityToOutDTO(service.update(dto));
    }
}
