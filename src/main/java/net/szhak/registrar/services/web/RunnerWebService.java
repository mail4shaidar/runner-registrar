package net.szhak.registrar.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.db.repo.RunnerRepository;
import net.szhak.registrar.services.generic.RunnerService;
import net.szhak.registrar.utils.dto.in.RunnerInDTO;
import net.szhak.registrar.utils.dto.out.RunnerOutDTO;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RunnerWebService extends AbstractWebService<RunnerEntity, RunnerInDTO, RunnerOutDTO, RunnerRepository, RunnerService> {
}
