package net.szhak.registrar.services.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.repo.RunRepository;
import net.szhak.registrar.services.generic.RunService;
import net.szhak.registrar.utils.dto.in.RunInDTO;
import net.szhak.registrar.utils.dto.out.RunOutDTO;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class RunWebService extends AbstractWebService<RunEntity, RunInDTO, RunOutDTO, RunRepository, RunService> {

    public void checkRunStatusAndDate(RunInDTO dto) {
        service.checkRunStatusAndDate(dto);
    }

    @Override
    public RunOutDTO create(RunInDTO dto) {
        checkRunStatusAndDate(dto);
        return super.create(dto);
    }

    @Override
    public RunOutDTO update(RunInDTO dto) {
        checkRunStatusAndDate(dto);
        return super.update(dto);
    }
}
