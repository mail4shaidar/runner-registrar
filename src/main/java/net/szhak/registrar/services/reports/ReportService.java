package net.szhak.registrar.services.reports;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.exceptions.FieldNotSpecifiedException;
import net.szhak.registrar.exceptions.InternalException;
import net.szhak.registrar.services.generic.RunService;
import net.szhak.registrar.services.generic.RunnerService;
import net.szhak.registrar.utils.dto.report.OneRunDTO;
import net.szhak.registrar.utils.dto.report.RunsForRunnerOutDTO;
import net.szhak.registrar.utils.dto.report.StatsForRunnerOutDTO;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReportService {

    private final RunService runService;
    private final RunnerService runnerService;

    /**
     * Метод получения списка всех забегов для конкретного пользователя (по его id) с возможностью фильтрации
     * по дате старта забега - from_datetime, to_datetime. В списке так же необходимо возвращать среднюю скорость
     * для каждого забега.
     */
    public RunsForRunnerOutDTO runsForRunner(Long id, LocalDate from, LocalDate to) {
        RunnerEntity runner = Optional.ofNullable(id)
                .map(runnerService::getById)
                .orElseThrow(() -> new InternalException("Couldn't find runner with id " + id));

        List<OneRunDTO> runs = getRunsForRunner(id, from, to);
        return RunsForRunnerOutDTO.builder()
                .firstName(runner.getFirstName())
                .lastName(runner.getLastName())
                .from(from)
                .to(to)
                .runs(runs)
                .build();
    }

    /**
     * Метод возвращения статистики по конкретному пользователю с возможностью фильтрации по дате старта забега -
     * from_datetime, to_datetime. Возвращать нужно количество совершенных забегов, общее количество метров и
     * среднюю скорость по всем забегам в выбранном периоде.
     */
    public StatsForRunnerOutDTO statsForRunner(Long id, LocalDate from, LocalDate to) {
        RunnerEntity runner = Optional.ofNullable(id)
                .map(runnerService::getById)
                .orElseThrow(() -> new InternalException("Couldn't find runner with id " + id));

        List<OneRunDTO> runs = getRunsForRunner(id, from, to);
        int amount = runs.size();
        long time = runs.stream().mapToLong(OneRunDTO::getTime).sum();
        double distance = runs.stream().mapToDouble(OneRunDTO::getDistance).sum();
        double speed = distance / time;

        return StatsForRunnerOutDTO.builder()
                .runs(runs)
                .runsCount(amount)
                .speed(speed)
                .distance(distance)
                .firstName(runner.getFirstName())
                .lastName(runner.getLastName())
                .from(from)
                .to(to)
                .build();
    }

    private List<OneRunDTO> getRunsForRunner(Long id, LocalDate from, LocalDate to) {
        if ((from == null && to != null) || (from != null && to == null)) {
            throw new FieldNotSpecifiedException("From and to date should be both null or both specified");
        }

        List<RunEntity> runs = runService.getRunsForRunner(id, from, to);
        if (!isEven(runs.size())) {
            runs.remove(runs.size() - 1);
        }

        List<OneRunDTO> dtos = new ArrayList<>();

        for (int i = 0; i < runs.size(); i += 2) {
            RunEntity start = runs.get(i);
            RunEntity finish = runs.get(i + 1);
            Double distance = finish.getDistance();
            long time = Duration.between(start.getDateTime(), finish.getDateTime()).toSeconds();
            double speed = distance / time;
            OneRunDTO dto = OneRunDTO.builder()
                    .distance(distance)
                    .time(time)
                    .speed(speed)
                    .build();
            dtos.add(dto);
        }

        return dtos;
    }

    private static boolean isEven(int x) {
        return (x ^ 1) > x;
    }
}
