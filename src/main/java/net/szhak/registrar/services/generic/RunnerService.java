package net.szhak.registrar.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.db.repo.RunnerRepository;
import net.szhak.registrar.exceptions.EntityNotFoundException;
import net.szhak.registrar.exceptions.FieldNotSpecifiedException;
import net.szhak.registrar.exceptions.MethodNotAllowedException;
import net.szhak.registrar.utils.dto.in.RunnerInDTO;
import net.szhak.registrar.utils.dto.out.RunnerOutDTO;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RunnerService extends AbstractService<RunnerEntity, RunnerInDTO, RunnerOutDTO, RunnerRepository> {

    @Override
    public RunnerEntity create(RunnerInDTO dto) {
        if (dto.getId() != null) {
            throw new MethodNotAllowedException("Providing ID is not allowed for runner creation");
        }
        return super.create(dto);
    }

    @Override
    public RunnerEntity update(RunnerInDTO dto) {
        if (dto.getId() == null) {
            throw new FieldNotSpecifiedException("Runner ID is not specified!");
        }

        Long runId = Optional.of(dto.getId()).orElseThrow(() -> new FieldNotSpecifiedException("Run ID is not specified!"));

        repository.findById(runId)
                .ifPresentOrElse(r -> log.debug("Updating runner {}", runId),
                        () -> {
                            throw new EntityNotFoundException("Runner not found by ID: " + runId);
                        }
                );

        return super.update(dto);
    }
}
