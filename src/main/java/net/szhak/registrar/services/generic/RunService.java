package net.szhak.registrar.services.generic;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.db.repo.RunRepository;
import net.szhak.registrar.db.repo.RunnerRepository;
import net.szhak.registrar.exceptions.EntityNotFoundException;
import net.szhak.registrar.exceptions.FieldNotSpecifiedException;
import net.szhak.registrar.exceptions.MethodNotAllowedException;
import net.szhak.registrar.exceptions.WrongRunStatusException;
import net.szhak.registrar.services.util.DistanceService;
import net.szhak.registrar.utils.Constants;
import net.szhak.registrar.utils.dto.in.RunInDTO;
import net.szhak.registrar.utils.dto.out.RunOutDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class RunService extends AbstractService<RunEntity, RunInDTO, RunOutDTO, RunRepository> {

    private final RunnerRepository runnerRepository;
    private final DistanceService distanceService;

    private final List<String> allowedEventTypes = List.of(Constants.EVENT_TYPE_START_RUN, Constants.EVENT_TYPE_START_FINISH);

    public void checkRunStatusAndDate(RunInDTO dto) {
        if (!allowedEventTypes.contains(dto.getType())) {
            throw new WrongRunStatusException("Run status must be one of these values: " + allowedEventTypes);
        }

        Long runnerId = Optional.ofNullable(dto.getRunnerId()).orElseThrow(() -> new FieldNotSpecifiedException("Runner ID is not specified!"));

        LocalDateTime dateTime = Optional.ofNullable(dto.getDateTime()).orElseThrow(() -> new FieldNotSpecifiedException("Run dateTime is not specified!"));
        boolean laterRecordsExist = repository.existsByRunnerIdAndDateTimeGreaterThanEqual(runnerId, dateTime);
        if (laterRecordsExist) throw new WrongRunStatusException("There are newer run events for runner " + runnerId);

        String runEventType = Optional.ofNullable(dto.getType()).orElseThrow(() -> new FieldNotSpecifiedException("Run event type is not specified!"));
        RunEntity lastRunEvent = repository.getFirstByRunnerIdOrderByIdDesc(runnerId);
        if (lastRunEvent == null) {
            if (!Constants.EVENT_TYPE_START_RUN.equals(runEventType)) {
                throw new WrongRunStatusException("Run event type should be " + Constants.EVENT_TYPE_START_RUN + " for the first run event");
            }
        } else {
            String lastRunEventType = lastRunEvent.getType();
            if (StringUtils.equals(runEventType, lastRunEventType)) {
                throw new WrongRunStatusException("Run event type should not be the same as previous! Now last event for runner " + runnerId + " is " + lastRunEventType);
            }
        }
    }

    @Override
    public RunEntity create(RunInDTO dto) {
        Long runnerId = Optional.ofNullable(dto.getRunnerId()).orElseThrow(() -> new FieldNotSpecifiedException("Runner ID is not specified!"));
        RunnerEntity runner = runnerRepository.findById(runnerId).orElseThrow(() -> new EntityNotFoundException("Runner not found by ID: " + runnerId));
        RunEntity entity = this.inDTOToEntity(dto);
        entity.setRunner(runner);

        if (Constants.EVENT_TYPE_START_FINISH.equals(dto.getType()) && dto.getDistance() == null) {
            RunEntity startEvent = Optional.ofNullable(this.getStartEventForNewFinish(dto)).orElseThrow(() -> new EntityNotFoundException("Couldn't find start event for this finish event: " + dto));
            double distance = distanceService.calculateDistance(startEvent.getLatitude(), startEvent.getLongitude(),
                    dto.getLatitude(), dto.getLongitude());
            entity.setDistance(distance);
        }

        return repository.save(entity);
    }

    @Override
    public RunEntity update(RunInDTO dto) {
        throw new MethodNotAllowedException("Updating run events information is not allowed");
    }

    @Override
    public RunOutDTO entityToOutDTO(RunEntity entity) {
        RunOutDTO dto = super.entityToOutDTO(entity);
        dto.setRunnerId(entity.getRunner().getId());
        return dto;
    }

    private RunEntity getStartEventForNewFinish(RunInDTO dto) {
        return repository.getFirstByRunnerIdOrderByIdDesc(dto.getRunnerId());
    }

    public List<RunEntity> getRunsForRunner(Long runnerId, LocalDate from, LocalDate to) {
        if (from == null && to == null) {
            return repository.getAllByRunnerIdOrderById(runnerId);
        } else {
            return repository.getAllByRunnerIdAndDateTimeBetweenOrderById(runnerId, from.atStartOfDay(), to.plusDays(1).atStartOfDay());
        }
    }
}
