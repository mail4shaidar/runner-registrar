package net.szhak.registrar.exceptions;

public class MethodNotAllowedException extends RegistrarAbstractException {
    public MethodNotAllowedException(String msg) {
        super(msg);
    }
}
