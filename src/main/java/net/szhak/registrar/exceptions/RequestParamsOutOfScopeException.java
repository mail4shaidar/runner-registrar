package net.szhak.registrar.exceptions;

public class RequestParamsOutOfScopeException extends RegistrarAbstractException {
    public RequestParamsOutOfScopeException(String msg) {
        super(msg);
    }
}
