package net.szhak.registrar.exceptions;

public class FieldNotSpecifiedException extends RegistrarAbstractException {
    public FieldNotSpecifiedException(String msg) {
        super(msg);
    }
}
