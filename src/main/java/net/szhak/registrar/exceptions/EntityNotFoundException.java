package net.szhak.registrar.exceptions;

public class EntityNotFoundException extends RegistrarAbstractException {
    public EntityNotFoundException(String msg) {
        super(msg);
    }
}
