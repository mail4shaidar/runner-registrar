package net.szhak.registrar.exceptions;

public abstract class RegistrarAbstractException extends RuntimeException {
    protected RegistrarAbstractException(String msg) {
        super(msg);
    }
}
