package net.szhak.registrar.exceptions;

public class WrongRunStatusException extends RegistrarAbstractException {
    public WrongRunStatusException(String msg) {
        super(msg);
    }
}
