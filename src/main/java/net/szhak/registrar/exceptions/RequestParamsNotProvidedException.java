package net.szhak.registrar.exceptions;

public class RequestParamsNotProvidedException extends RegistrarAbstractException {
    public RequestParamsNotProvidedException(String msg) {
        super(msg);
    }
}
