package net.szhak.registrar.exceptions;

public class InternalException extends RegistrarAbstractException {
    public InternalException(String msg) {
        super(msg);
    }
}
