package net.szhak.registrar.db.repo;

import net.szhak.registrar.db.ent.RunnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RunnerRepository extends JpaRepository<RunnerEntity, Long> {
}
