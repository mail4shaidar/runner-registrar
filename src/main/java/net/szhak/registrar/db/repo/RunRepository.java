package net.szhak.registrar.db.repo;

import net.szhak.registrar.db.ent.RunEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RunRepository extends JpaRepository<RunEntity, Long> {

    @Query("select (count(r) > 0) from RUN r where r.runner.id = :runnerId and r.dateTime >= :dt")
    boolean existsByRunnerIdAndDateTimeGreaterThanEqual(@Param("runnerId") Long runnerId, @Param("dt") LocalDateTime dt);

    @Query("select r from RUN r where r.runner.id = :runnerId order by r.id DESC limit 1")
    RunEntity getFirstByRunnerIdOrderByIdDesc(@Param("runnerId") Long runnerId);

    @Query("select r from RUN r where r.runner.id = :runnerId order by r.id")
    List<RunEntity> getAllByRunnerIdOrderById(@Param("runnerId") Long runnerId);

    @Query("select r from RUN r where r.runner.id = :runnerId and r.dateTime between :from and :to order by r.id")
    List<RunEntity> getAllByRunnerIdAndDateTimeBetweenOrderById(@Param("runnerId") Long runnerId, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);
}
