package net.szhak.registrar.config.jackson.date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class JsonISOLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime deserialize(JsonParser jsonParser,
                                     DeserializationContext context) throws IOException {
        final String text = jsonParser.getValueAsString();
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return ZonedDateTime.parse(text).toLocalDateTime();
    }
}