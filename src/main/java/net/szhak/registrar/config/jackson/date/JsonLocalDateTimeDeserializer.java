package net.szhak.registrar.config.jackson.date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static net.szhak.registrar.utils.Constants.LOCAL_DATE_TIME_FORMAT;

@Slf4j
public class JsonLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LOCAL_DATE_TIME_FORMAT);

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final ObjectCodec objectCodec = jsonParser.getCodec();
        final TextNode node = objectCodec.readTree(jsonParser);
        final String text = node.textValue();
        if (StringUtils.isBlank(text)) {
            return null;
        }
        return LocalDateTime.parse(text, formatter);
    }
}