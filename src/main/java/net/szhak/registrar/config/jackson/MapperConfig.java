package net.szhak.registrar.config.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.config.jackson.date.JsonLocalDateDeserializer;
import net.szhak.registrar.config.jackson.date.JsonLocalDateSerializer;
import net.szhak.registrar.config.jackson.date.JsonLocalDateTimeDeserializer;
import net.szhak.registrar.config.jackson.date.JsonLocalDateTimeSerializer;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS;
import static net.szhak.registrar.utils.Constants.LOCAL_DATE_TIME_WITH_TIMEZONE_FORMAT;

@Slf4j
@Configuration
@AutoConfigureBefore(JacksonAutoConfiguration.class)
public class MapperConfig {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat(LOCAL_DATE_TIME_WITH_TIMEZONE_FORMAT));
        objectMapper.getSerializationConfig().withFeatures(WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // LocalDate
        SimpleModule localDateModule = new SimpleModule();
        localDateModule.addDeserializer(
                LocalDate.class,
                new JsonLocalDateDeserializer()
        );
        localDateModule.addSerializer(
                LocalDate.class,
                new JsonLocalDateSerializer()
        );
        objectMapper.registerModule(localDateModule);

        // LocalDateTime
        SimpleModule localDateTimeModule = new SimpleModule();
        localDateTimeModule.addSerializer(
                LocalDateTime.class,
                new JsonLocalDateTimeSerializer()
        );
        localDateTimeModule.addDeserializer(
                LocalDateTime.class,
                new JsonLocalDateTimeDeserializer()
        );
        objectMapper.registerModule(localDateTimeModule);

        return objectMapper;
    }
}
