package net.szhak.registrar.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.db.repo.RunnerRepository;
import net.szhak.registrar.services.generic.RunnerService;
import net.szhak.registrar.services.web.RunnerWebService;
import net.szhak.registrar.utils.dto.in.RunnerInDTO;
import net.szhak.registrar.utils.dto.out.RunnerOutDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/runner")
@RequiredArgsConstructor
@Slf4j
public class RunnerController extends AbstractController<RunnerEntity, RunnerInDTO, RunnerOutDTO, RunnerRepository, RunnerService, RunnerWebService> {
}
