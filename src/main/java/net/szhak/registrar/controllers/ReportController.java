package net.szhak.registrar.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.services.web.ReportWebService;
import net.szhak.registrar.utils.Constants;
import net.szhak.registrar.utils.dto.report.RunsForRunnerOutDTO;
import net.szhak.registrar.utils.dto.report.StatsForRunnerOutDTO;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/reports")
@RequiredArgsConstructor
@Slf4j
public class ReportController {

    private final ReportWebService service;

    @GetMapping("/runsForRunner")
    public ResponseEntity<RunsForRunnerOutDTO> runsForRunner(@RequestParam(value="id") Long id,
                                                             @RequestParam(value="from", required = false) @DateTimeFormat(pattern = Constants.LOCAL_DATE_FORMAT) LocalDate from,
                                                             @RequestParam(value="to", required = false) @DateTimeFormat(pattern = Constants.LOCAL_DATE_FORMAT) LocalDate to) {
        return ResponseEntity.ok(service.runsForRunner(id, from, to));
    }

    @GetMapping("/statsForRunner")
    public ResponseEntity<StatsForRunnerOutDTO> statsForRunner(@RequestParam(value="id") Long id,
                                                               @RequestParam(value="from", required = false) @DateTimeFormat(pattern = Constants.LOCAL_DATE_FORMAT) LocalDate from,
                                                               @RequestParam(value="to", required = false) @DateTimeFormat(pattern = Constants.LOCAL_DATE_FORMAT) LocalDate to) {
        return ResponseEntity.ok(service.statsForRunner(id, from, to));
    }
}
