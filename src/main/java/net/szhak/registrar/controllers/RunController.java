package net.szhak.registrar.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.repo.RunRepository;
import net.szhak.registrar.services.generic.RunService;
import net.szhak.registrar.services.web.RunWebService;
import net.szhak.registrar.utils.dto.in.RunInDTO;
import net.szhak.registrar.utils.dto.out.RunOutDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/run")
@RequiredArgsConstructor
@Slf4j
public class RunController extends AbstractController<RunEntity, RunInDTO, RunOutDTO, RunRepository, RunService, RunWebService> {
}
