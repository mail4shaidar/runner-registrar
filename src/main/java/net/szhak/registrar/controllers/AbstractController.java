package net.szhak.registrar.controllers;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.AbstractEntity;
import net.szhak.registrar.services.generic.AbstractService;
import net.szhak.registrar.services.web.AbstractWebService;
import net.szhak.registrar.utils.dto.in.AbstractInDTO;
import net.szhak.registrar.utils.dto.out.AbstractOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
public abstract class AbstractController<E extends AbstractEntity,
        I extends AbstractInDTO,
        O extends AbstractOutDTO,
        R extends JpaRepository<E, Long>,
        S extends AbstractService<E, I, O, R>,
        W extends AbstractWebService<E, I, O, R, S>> {

    protected W service;

    @Autowired
    private void setService(W service) {
        this.service = service;
    }

    @PostConstruct
    private void init() {
        log.info("Initializing: {}", this.getClass().getSimpleName());
    }

    @GetMapping("/all")
    public ResponseEntity<List<O>> getAll(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer size) {
        return ResponseEntity.ok(service.getAll(page, size));
    }

    @GetMapping("/{id}")
    public ResponseEntity<O> getById(@PathVariable Long id) {
        return ResponseEntity.ok(service.getById(id));
    }

    @PutMapping
    public ResponseEntity<O> createPost(@RequestBody I dto) {
        return ResponseEntity.ok(service.create(dto));
    }

    @PostMapping
    public ResponseEntity<O> updatePost(@RequestBody I dto) {
        return ResponseEntity.ok(service.update(dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
