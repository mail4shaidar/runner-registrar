package net.szhak.registrar.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String LOCAL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String LOCAL_DATE_TIME_FORMAT = DATE_FORMAT;
    public static final String LOCAL_DATE_TIME_WITH_TIMEZONE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSXXX";

    public static final String EVENT_TYPE_START_RUN = "start";
    public static final String EVENT_TYPE_START_FINISH = "finish";
}
