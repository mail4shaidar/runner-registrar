package net.szhak.registrar.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class RunnerOutDTO extends AbstractOutDTO {
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String sex;
}
