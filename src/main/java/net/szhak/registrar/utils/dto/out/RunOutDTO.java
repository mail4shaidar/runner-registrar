package net.szhak.registrar.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class RunOutDTO extends AbstractOutDTO {
    private Long runnerId;
    private Double latitude;
    private Double longitude;
    private Double distance;
    private LocalDateTime dateTime;
    private String type;
}
