package net.szhak.registrar.utils.dto.report;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class OneRunDTO {
    private Long time;
    private Double distance;
    private Double speed;
}
