package net.szhak.registrar.utils.dto.in;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import net.szhak.registrar.utils.dto.AbstractDTO;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractInDTO implements AbstractDTO {
    private Long id;
}
