package net.szhak.registrar.utils.dto.report;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.List;

@Setter
@Getter
@SuperBuilder
public class AbstractReportDTO {
    private String firstName;
    private String lastName;
    private LocalDate from;
    private LocalDate to;
    private List<OneRunDTO> runs;
}
