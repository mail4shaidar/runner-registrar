package net.szhak.registrar.utils.dto.report;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class RunsForRunnerOutDTO extends AbstractReportDTO {
}
