package net.szhak.registrar.utils.dto.in;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public class RunInDTO extends AbstractInDTO {
    @NotNull
    private Long runnerId;
    @NotNull
    private Double latitude;
    @NotNull
    private Double longitude;
    private Double distance;
    @NotNull
    private LocalDateTime dateTime;
    @NotNull
    private String type;
}
