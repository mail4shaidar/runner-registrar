package net.szhak.registrar.utils.dto.report;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class StatsForRunnerOutDTO extends AbstractReportDTO {
    private double runsCount;
    private double distance;
    private double speed;
}
