package net.szhak.registrar.utils.dto.out;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import net.szhak.registrar.utils.dto.AbstractDTO;
import org.springframework.lang.NonNull;

@SuperBuilder
@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractOutDTO implements AbstractDTO {
    @NonNull
    private Long id;
}
