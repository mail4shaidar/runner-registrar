package net.szhak.registrar.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static net.szhak.registrar.utils.Constants.LOCAL_DATE_TIME_FORMAT;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateTimeHelperUtils {

    public static String localDateTimeToString(LocalDateTime dt) {
        if (dt == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LOCAL_DATE_TIME_FORMAT);
        return dt.format(formatter);
    }
}
