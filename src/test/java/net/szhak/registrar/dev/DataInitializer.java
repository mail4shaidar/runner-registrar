package net.szhak.registrar.dev;

import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.services.generic.RunService;
import net.szhak.registrar.services.generic.RunnerService;
import net.szhak.registrar.utils.Constants;
import net.szhak.registrar.utils.dto.in.RunInDTO;
import net.szhak.registrar.utils.dto.in.RunnerInDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class DataInitializer {

    private final RunnerService runnerService;
    private final RunService runService;

    public static final Faker faker = new Faker();
    public static final Random random = new Random();

    public RunnerEntity validRunner;
    public RunEntity validRun;

    public static final Long LEGAL_MAX_ID = 10L;
    public static final Long ILLEGAL_MAX_ID = LEGAL_MAX_ID * LEGAL_MAX_ID + 2;

    public void initData() {
        initializeRunners();
        initializeRuns();
    }

    private void initializeRunners() {
        for (int i = 0; i < LEGAL_MAX_ID; i++) {
            RunnerInDTO dto = RunnerInDTO.builder()
                    .sex(faker.demographic().sex())
                    .birthDate(getRandomBirthDate())
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .build();
            runnerService.create(dto);
        }
    }

    private void initializeRuns() {
        for (long i = 0; i < LEGAL_MAX_ID; i++) { //runners
            LocalDateTime ldt = LocalDateTime.now();
            String type = Constants.EVENT_TYPE_START_RUN;
            for (int j = 0; j < LEGAL_MAX_ID; j++) { //runs
                RunInDTO dto = RunInDTO.builder()
                        .runnerId(i + 1)
                        .dateTime(ldt.plusMinutes(j))
                        .latitude(random.nextDouble(10000))
                        .longitude(random.nextDouble(10000))
                        .type(type)
                        .build();
                runService.create(dto);
                if (Constants.EVENT_TYPE_START_RUN.equals(type)) {
                    type = Constants.EVENT_TYPE_START_FINISH;
                } else {
                    type = Constants.EVENT_TYPE_START_RUN;
                }
            }
        }
    }

    public static LocalDate getRandomBirthDate() {
        LocalDate now = LocalDate.now();
        return now.minusYears(30).plusDays(random.nextLong(365) - 182);
    }

}
