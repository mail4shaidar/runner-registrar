package net.szhak.registrar.controllers;

import jakarta.annotation.PostConstruct;
import net.szhak.registrar.db.ent.RunEntity;
import net.szhak.registrar.db.repo.RunRepository;
import net.szhak.registrar.utils.Constants;
import net.szhak.registrar.utils.dto.in.RunInDTO;

import java.time.LocalDateTime;

import static net.szhak.registrar.dev.DataInitializer.*;

public class RunControllerTests extends AbstractControllerTests<RunEntity, RunInDTO, RunRepository> {

    @PostConstruct
    private void init() {
        IN_DTO_CREATE = RunInDTO.builder()
                .runnerId(1L)
                .dateTime(LocalDateTime.now().plusMinutes(LEGAL_MAX_ID+1))
                .latitude(random.nextDouble(10000))
                .longitude(random.nextDouble(10000))
                .type(Constants.EVENT_TYPE_START_RUN)
                .build();
        REQUEST_MAPPING = "/run";
    }
}
