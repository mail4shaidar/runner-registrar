package net.szhak.registrar.controllers;

import jakarta.annotation.PostConstruct;
import net.szhak.registrar.db.ent.RunnerEntity;
import net.szhak.registrar.db.repo.RunnerRepository;
import net.szhak.registrar.utils.dto.in.RunnerInDTO;

import static net.szhak.registrar.dev.DataInitializer.faker;
import static net.szhak.registrar.dev.DataInitializer.getRandomBirthDate;

public class RunnerControllerTests extends AbstractControllerTests<RunnerEntity, RunnerInDTO, RunnerRepository> {

    @PostConstruct
    private void init() {
        IN_DTO_CREATE = RunnerInDTO.builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .sex(faker.demographic().sex())
                .birthDate(getRandomBirthDate())
                .build();
        REQUEST_MAPPING = "/runner";
    }
}
